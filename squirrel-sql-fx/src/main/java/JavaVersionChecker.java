import javax.swing.JOptionPane;

public class JavaVersionChecker {
    private static final String JAVA_HOME_PROPERTY = "java.home";
    private static final String JAVA_VERSION_PROPERTY = "java.version";

    public JavaVersionChecker() {
    }

    private static boolean checkVersion(String var0, String[] var1) {
        if(var0 == null) {
            System.err.println("jvm version could not be determined. The java.version system property is null");
        }

        boolean var2 = false;

        for(int var3 = 0; var3 < var1.length; ++var3) {
            if(var0.startsWith(var1[var3])) {
                var2 = true;
            }
        }

        return var2;
    }

    public static void main(String[] args) {
        if(args.length == 0) {
            System.err.println("JavaVersionChecker: Must specify one or more minimum JVM versions");
            System.exit(1);
        }

        final String currentVersion = System.getProperty(JAVA_VERSION_PROPERTY);
        if(!checkVersion(currentVersion, args)) {
            final String javaHome = System.getProperty(JAVA_HOME_PROPERTY);
            JOptionPane.showMessageDialog(null, "Your Java Virtual Machine must be at least " + args[0] + " to run SQuirreL FX\n" + "  JVM Version used: " + currentVersion + "\n" + "  JVM Location: " + javaHome);
            System.exit(1);
        }

        System.exit(0);
    }
}